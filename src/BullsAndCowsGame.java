import java.util.Scanner;

public class BullsAndCowsGame {

        private static String computerSecretCode = "";
        private static String playerSecretCode ="";

        private static void printWelcomeMessage() {
            System.out.println("Welcome to the game of BULLS and COWS.");
            System.out.println("The objective in this game is for you to guess the computer's 4-digit number before the computer guesses your 4-digit number.");
            System.out.println("You have a maximum of 7 guesses.");
            System.out.println("The computer responds with how close your guess is to the target number.");
            System.out.println("BULLS = # correct digits in the correct position.");
            System.out.println("COWS = # correct digits in the wrong position.");
            System.out.println();
        }

        private static String produceRandomTarget() {
            int randomNumber = 1000 + ((int) (Math.random() * 10000) % 9000);
            while( hasRepeatingDigits(randomNumber+ "")){
                randomNumber = 1000 + ((int) (Math.random() * 10000) % 9000);
            }

            if (computerSecretCode.equals("")) {
                computerSecretCode = randomNumber + "";
            }
            else {return randomNumber + "";}
            return null;
        }

        private static boolean hasRepeatingDigits(String num) {
            for (int i = 0; i < num.length() - 1; i++) {
                for (int j = i + 1; j < num.length(); j++) {
                    if (num.charAt(i) == num.charAt(j)) {
                        return true;
                    }
                }
            }
            return false;
        }

        private static boolean containsNonDigits(String num) {
            if (!num.matches("^[0-9]+$")) {
                return true;
            }
            return false;
        }

        private static int computeBulls(String num1, String num2) {
            int bullCounter = 0;

            for (int i = 0; i < 4; i++) {
                if (num1.charAt(i) == num2.charAt(i)) {
                    bullCounter++;
                }
            }
            System.out.println(num1 + " " + num2);
            return bullCounter;
        }

        private static int computeCows(String num1, String num2) {
            int cowsCounter = 0;
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    if (i != j) {
                        if (num1.charAt(i) == num2.charAt(j)) {
                            cowsCounter++;
                        }
                    }
                }
            }
            return cowsCounter;
        }

        public static boolean inputValidation () {
            Scanner sc = new Scanner(System.in);
            String guessedNumber = sc.nextLine();
//            if(playerSecretCode.equals("")) {
//                playerSecretCode = guessedNumber;
//            }
            if (hasRepeatingDigits(guessedNumber)) {
                System.out.println("Your number should not contain repeating digits.");
            } else if (guessedNumber.length() != 4) {
                System.out.println("Your number should contain 4 symbols (Digits)");
            } else if (containsNonDigits(guessedNumber)) {
                System.out.println("Your number should not contain non-digits.");
            }
            else {
                if(playerSecretCode.equals("")) {
                    playerSecretCode = guessedNumber;
                }
                return true;
            }
            return false;
        }

        public static void main(String[] args) {
            Scanner sc = new Scanner(System.in);
            printWelcomeMessage();
            produceRandomTarget();

            int bulls = 0;
            int cows = 0;
            int guesses = 1;
            boolean notFound = true;
            boolean codeGood = false;
            String computerChoice = "";//MODIFIED

            while (!codeGood) {
                System.out.print("Enter your secret 4-digit number: ");
                codeGood = inputValidation();
            }

//            playerSecretCode = sc.nextLine(); MODIFICATION
            System.out.println("Your secret 4-digit number is " + playerSecretCode);

            while (notFound) {

                System.out.print("Enter guess number " + guesses + ": ");
                String guessedNumber = sc.nextLine();
                bulls = computeBulls(guessedNumber, computerSecretCode);
                cows = computeCows(guessedNumber, computerSecretCode);

                computerChoice = produceRandomTarget();//MODIFIED
                System.out.println("The computer guessed " + computerChoice);

                if (guesses >= 7) {
                    System.out.println("Bulls = " + bulls + "\tCows = " + cows);
                    System.out.println("Sorry! You ran out of guesses.");
                    System.out.println("The correct number was: " + computerSecretCode);
                    notFound = false;
                } else if (hasRepeatingDigits(guessedNumber)) {
                    System.out.println("Your guess should not contain repeating digits.");
                } else if (guessedNumber.length() != 4) {
                    System.out.println("Your guess should contain 4 symbols (Digits)");
                } else if (containsNonDigits(guessedNumber)) {
                    System.out.println("Your guess should not contain non-digits.");
                } else if (bulls == 4) {
                    System.out.println("Bulls = " + bulls + "\tCows = " + cows);
                    System.out.println("Congratulations! You won with " + guesses + " guesses.");
                    notFound = false;
                } else if (computerChoice.equals(playerSecretCode)) {
                    System.out.println("The computer guessed your number. The computer wins!");
                    notFound = false;
                } else if (!hasRepeatingDigits(guessedNumber) && !containsNonDigits(guessedNumber)) {
                    System.out.println("Bulls = " + bulls + "\tCows = " + cows);
                    guesses++;
                }
            }
            sc.close();
        }
    }

